﻿using System.Data.SqlClient;

namespace DAO.Interfaces
{
    public interface IEstablecimiento
    {
        SqlDataReader GetEstablishmentById(int id);
    }
}