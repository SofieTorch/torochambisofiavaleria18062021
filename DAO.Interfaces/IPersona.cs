﻿using System.Data;

namespace DAO.Interfaces
{
    public interface IPersona
    {
        DataTable GetPersonByNameMatch(string criteria, bool onlyName);
        int ReplaceNameByMatch(string criteria, string replace);
    }
}