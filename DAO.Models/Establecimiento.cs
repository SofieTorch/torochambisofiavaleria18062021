﻿namespace DAO.Models
{
    public class Establecimiento
    {
        public string RazonSocial { get; set; }
        public string Clase { get; set; }  
        public string NombrePropietario { get; set; }
        public string TelefonoFijo { get; set; }
        public string TelefonoCelular { get; set; }
        public int AnioApertura { get; set; }
        public string Provincia { get; set; }
        
        public Establecimiento(string razonSocial, string clase, string nombrePropietario, string telefonoFijo, string telefonoCelular, int anioApertura, string provincia)
        {
            RazonSocial = razonSocial;
            Clase = clase;
            NombrePropietario = nombrePropietario;
            TelefonoFijo = telefonoFijo;
            TelefonoCelular = telefonoCelular;
            AnioApertura = anioApertura;
            Provincia = provincia;
        }
    }
}