﻿namespace DAO.Models
{
    public class Persona
    {
        public string NombreCompleto { get; set; }
        public string Ci { get; set; }
    }
}