﻿using System;
using System.Data.SqlClient;
using DAO.Interfaces;

namespace DAO.Implementation
{
    public class EstablecimientoImpl : IEstablecimiento
    {
        public SqlDataReader GetEstablishmentById(int id)
        {
            string query = @"
                IF EXISTS (SELECT idEstablecimiento FROM bdempresas.establecimiento WHERE idEstablecimiento = 1097)
                    SELECT E.razonSocial, C.nombreClase, CONCAT(P.primerApellido, ' ', P.segundoApellido, ' ', P.nombres),
                            ISNULL(E.telefonoFijo, 'SIN telefono fijo'),
                            ISNULL(E.telefonoCelular, 'SIN Celular'),
                            YEAR(E.fechaApertura), Pr.nombreProvincia
                    FROM bdempresas.establecimiento E
                    INNER JOIN bdempresas.clase C ON E.idClase = C.idClase
                    INNER JOIN bdempresas.persona P ON E.idPersona = P.idPersona
                    INNER JOIN bdempresas.municipio M ON E.idMunicipio = M.idMunicipio
                    INNER JOIN bdempresas.provincia Pr ON M.idProvincia = Pr.idProvincia
                    WHERE E.idEstablecimiento = 1097
                ELSE 
                    SELECT 'NO Existe el establecimiento'";
            try
            {
                DatabaseConnection dbconn = new DatabaseConnection();
                SqlCommand command = dbconn.CreateCommand(query);
                command.Parameters.AddWithValue("@id", id);
                return DatabaseConnection.ExecuteDataReaderCommand(command);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}