﻿using System;
using System.Data;
using System.Data.SqlClient;
using DAO.Interfaces;

namespace DAO.Implementation
{
    public class PersonaImpl : IPersona
    {
        public DataTable GetPersonByNameMatch(string criteria, bool onlyName)
        {
            string query;
            if (onlyName)
            {
                query = @"
                SELECT ci, CONCAT(primerApellido, ' ', segundoApellido, ' ', nombres) AS 'Nombre Completo'
                FROM bdempresas.persona
                WHERE nombres = @criteria";
            }
            else {
                query = @"
                SELECT ci, CONCAT(primerApellido, ' ', segundoApellido, ' ', nombres) AS 'Nombre Completo'
                FROM bdempresas.persona
                WHERE CONCAT(primerApellido, ' ', segundoApellido, ' ', nombres) LIKE CONCAT('%', @criteria, '%')";
                
            }
            try
            {
                DatabaseConnection dbconn = new DatabaseConnection();
                SqlCommand command = dbconn.CreateCommand(query);
                command.Parameters.AddWithValue("@criteria", criteria);
                return DatabaseConnection.ExecuteSelectCommand(command);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int ReplaceNameByMatch(string criteria, string replace)
        {
            string query = @"
                UPDATE bdempresas.persona SET
                nombres = @replace
                WHERE CONCAT(primerApellido, ' ', segundoApellido, ' ', nombres) LIKE CONCAT('%', @criteria, '%')";
            try
            {
                DatabaseConnection dbconn = new DatabaseConnection();
                SqlCommand command = dbconn.CreateCommand(query);
                command.Parameters.AddWithValue("@criteria", criteria);
                command.Parameters.AddWithValue("@replace", replace);
                return DatabaseConnection.ExecuteCommand(command);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}