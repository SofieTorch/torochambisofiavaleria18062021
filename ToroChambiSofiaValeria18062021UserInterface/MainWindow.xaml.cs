﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows;
using DAO.Implementation;

namespace ToroChambiSofiaValeria18062021UserInterface
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        private PersonaImpl _implPersona;
        private EstablecimientoImpl _implEstablecimiento;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void BtnSearchPersonsLike_OnClick(object sender, RoutedEventArgs e)
        {
            string criteria = TxbCriteria.Text;
            LoadDataGrid(criteria, false);
        }

        private void BtnReplacePersonsLike_OnClick(object sender, RoutedEventArgs e)
        {
            string criteria = TxbCriteria.Text;
            string replace = TxbReplace.Text;

            try
            {
                _implPersona = new PersonaImpl();
                int res = _implPersona.ReplaceNameByMatch(criteria, replace);
                LblResult.Content = $"Se han actualizado {res} registros";
                if (res > 0)
                {
                    LoadDataGrid(replace, true);
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void LoadDataGrid(string criteria, bool onlyName)
        {
            try
            {
                _implPersona = new PersonaImpl();
                DataTable dt = _implPersona.GetPersonByNameMatch(criteria, onlyName);
                DataGridResults.ItemsSource = dt.DefaultView;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void BtnSearchEstablishment_OnClick(object sender, RoutedEventArgs e)
        {
            int id = Int32.Parse(TxbIdEst.Text);
            try
            {
                _implEstablecimiento = new EstablecimientoImpl();
                SqlDataReader dr = _implEstablecimiento.GetEstablishmentById(id);
                dr.Read();
                if (dr[0].ToString().Equals("NO Existe el establecimiento"))
                {
                    LblEstablecimientoResult.Content = dr[0];
                }
                else
                {
                    LblEstablecimientoResult.Content = $"{dr[0]}, {dr[1]}, {dr[2]}, {dr[3]}, {dr[4]}, {dr[5]}, {dr[6]}";
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}